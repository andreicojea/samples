###*
@ngdoc directive
@name modalBody
@module app
@restrict C

@description
Applies a maximum height to modals, in order to keep them inside the viewport.
If the modal content is long, the modal body area will have a scroll bar.

The maximum height will be applied to the modal body, to avoid using hacky `position: absolute`
on modal header, footer and body. The way it's done is by getting the window size,
taking in consideration a top margin and a bottom margin, substract the header and footer height,
then use this value to apply a css `max-height` on the `.modal-body`.

For convenience, a `C` restriction has been used, so it's a class based directive.
Given that all modals do have this header + body + footer structure and the `.modal-body` element,
this directive can be instantly applied to all existing modals. While this is against our
general practice regarding the directive types, the benefit of using class type is obvious.

@usage
No need to do anything special, just make sure your modal template has a `.modal-body` as always.
###

angular.module "App"

.directive "modalBody", ["$timeout", ($timeout) ->
  restrict: "C"
  link: (scope, elem, attrs) ->
    # reference the dom elements once, in the beginning
    modalElem = elem.closest ".modal-content"
    modalSiblinngs = elem.siblings()
    modalScroll = elem.children ".modal-scroll-area"

    modalMargin = 20 # top & bottom
    siblingsHeight = null

    ###*
    @ngdoc method
    @name modalBody#getSiblingsTotalHeight
    @description Calculates the total height of modal header, subheader, footer, etc
    @returns {integer} The total height of .modal-body siblings
    ###
    getSiblingsTotalHeight = ->
      totalHeight = 0
      modalSiblinngs.each ->
        totalHeight += $(this).outerHeight()
      totalHeight # return

    ###*
    @ngdoc method
    @name modalBody#updateMaxHeight
    @description The main update fn, computes and applies the `max-height`
    ###
    updateMaxHeight = ->
      # the sum of header & footer heights, plus top & bottom margin
      siblingsHeight = getSiblingsTotalHeight()
      substractHeight = siblingsHeight + 2 * modalMargin

      # compute maximum height of the modal body, apply the css
      maxHeight = $(window).height() - substractHeight
      elem.css maxHeight: maxHeight

      bodyPadding = parseInt(elem.css("padding-top")) + parseInt(elem.css("padding-bottom"))
      maxScrollHeight = maxHeight - bodyPadding
      modalScroll.css maxHeight: maxScrollHeight


    delayedUpdateMaxHeight = _.debounce updateMaxHeight, 200

    # init, do it with a timeout
    $timeout updateMaxHeight

    # update when the window gets resized
    $(window).on "resize", delayedUpdateMaxHeight

    # destroy, kindly unbind `window` events
    scope.$on "$destroy", ->
      $(window).off "resize", delayedUpdateMaxHeight

]