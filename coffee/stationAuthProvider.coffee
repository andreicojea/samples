###*
@ngdoc provider
@name stationAuthProvider
@module App
@description

Used for configuring {@link App.stationAuth stationAuth} service.

@example
```
stationAuthProvider
  .tokenTimeout 2 * 1000 # 2s
  .stationName -> "Test Station Name"
  .stationWidth -> 1024
  .stationHeight -> 768
```
###


###

stationAuthProvider API:
-------------------------

  .tokenTimeout(i)
    - sets the interval `i` for requesting the token, while the pin
      has not been authorized yet

  .stationWidth(fn)
    - fn returns the station width, by default it's the window width

  .stationHeight(fn)
    - fn returns the station height, by default it's the window height

  .stationName(fn)
    - fn returns the station name, by default it's "Untitled Device"


stationAuth API:
-----------------

  .authorize()
    - triggers the authorization process, gets the pin and token
    - it gets resolved when both the pin and the token are ready

  .getToken()
    - returns the token of an authorized station

  .isAuthorized()
    - tells if the station is authorized, meaning that it has a token

  .reset()
    - removes existing pin and token

stationAuth EVENTS:
--------------------

  stationAuth:exchange-pin
    - a pin has been exchanged for a token, show the pin to the user

###

angular.module "App"

.provider "stationAuth", ->
  tokenTimeout = 2 * 1000 # retry every 2s by default
  stationWidthFn = -> $(window).width()
  stationHeightFn = -> $(window).height()
  stationNameFn = -> "Untitled Device"

  ###*
  @ngdoc method
  @name stationAuthProvider#tokenTimeout
  @description Sets the interval for requesting the token.
  @param {Integer} i The interval.
  @returns {Object} self
  ###
  @tokenTimeout = (i) ->
    tokenTimeout = i
    return this

  ###*
  @ngdoc method
  @name stationAuthProvider#stationWidth
  @description Provide station width, by default it's the window width.
  @param {Function} fn Returns station width.
  @returns {Object} self
  ###
  @stationWidth = (fn) ->
    stationWidthFn = fn
    return this

  ###*
  @ngdoc method
  @name stationAuthProvider#stationHeight
  @description Provide station height, by default it's the window height.
  @param {Function} fn Returns station height.
  @returns {Object} self
  ###
  @stationHeight = (fn) ->
    stationHeightFn = fn
    return this

  ###*
  @ngdoc method
  @name stationAuthProvider#stationName
  @description Provide station name, by default it's "Untitled Device".
  @param {Function} fn Returns station name.
  @returns {Object} self
  ###
  @stationName = (fn) ->
    stationNameFn = fn
    return this

  ###*
  @ngdoc service
  @name stationAuth
  @module App
  @description

  This service authenticating the station, asking for a pin,
  then exchanging the pin for a token. It stores the pin and
  the token in $localStorage, so it knows if the station
  is already authenticated even after restarting the app.

  You can setup station properties and token request interval using
  the {@link App.stationAuthProvider stationAuthProvider} API.

  @requires $localStorage

  @example
  ```
  stationAuth.authorize()
    .then (stationData) ->
      # success, go to loading state
    .catch (reason) ->
      # failed, go to offline state
  ```
  ###

  ###*
  @ngdoc event
  @name stationAuth#stationAuth:exchange-pin
  @eventType broadcast on root scope
  @description A pin has been exchanged for a token.
  @param {String} stationPIN The current pin.
  ###

  @$get = ($timeout, $q, Rest, $localStorage, $rootScope) ->

    # promise & timeout variables declared first
    pinDeferred = $q.defer()
    tokenDeferred = $q.defer()
    requestTokenTimeout = null

    # on first run, create the stationAuth in the localStorage
    $localStorage.stationAuth ?=
      pin: null
      token: null

    ###*
    @ngdoc method
    @name stationAuth#reset
    @description Removes existing pin and token, clears timeouts, creates new promises.
    ###
    reset = ->
      # reset cached data
      $localStorage.stationAuth =
        pin: null
        token: null
      # cancel timeouts
      $timeout.cancel requestTokenTimeout
      # reset promisses
      pinDeferred = $q.defer()
      tokenDeferred = $q.defer()

    ###
    @description Requests a new pin unless there is already a cached one.
    @returns {Promise} Promise resolved when the pin is available.
    ###
    requestPIN = ->
      if $localStorage.stationAuth.pin?
        # resolve with a cached pin
        pinDeferred.resolve $localStorage.stationAuth.pin
      else
        # request new pin from api
        Rest.all("stations/request_pin").post
          wrapper: "station"
          name: stationNameFn()
          height: stationHeightFn()
          width: stationWidthFn()
        .then (resp) ->
          # success, store in the cache, resolve
          $localStorage.stationAuth.pin = resp.pin
          pinDeferred.resolve resp.pin
        .catch (reason) ->
          # failed, reject and abort
          pinDeferred.reject reason

      pinDeferred.promise # return

    ###
    @description Requests a new token unless there is already a cached one.
    @param {Object} opts Options:
      - `stationPIN`: the pin
      - `stationName`: the name
    @returns {Promise} Promise resolved when the token is available.
    ###
    requestToken = (opts) ->
      if $localStorage.stationAuth.token?
        # resolve with a cached token
        tokenDeferred.resolve $localStorage.stationAuth.token
      else
        # request a new token, exchange the pin
        $rootScope.$broadcast "stationAuth:exchange-pin", opts.stationPIN
        Rest.all('stations').customPUT({}, 'exchange', {pin: opts.stationPIN})
          .then (rest) ->
            # success, store in the cache, delete pin, resolve
            $localStorage.stationAuth.token = rest.station_token
            $localStorage.stationAuth.pin = null
            tokenDeferred.resolve rest.station_token
          .catch (reason) ->
            # failed, is the pin authorized?
            if reason.status is 401
              # not yet, token not ready
              console.log "
                token not ready, retry in #{tokenTimeout/1000}s.
                reason: #{reason.status}
              "
              requestTokenTimeout = $timeout ->
                requestToken opts
              , tokenTimeout
            else
              # other reason for failure
              tokenDeferred.reject reason

      tokenDeferred.promise # return

    ###*
    @ngdoc method
    @name stationAuth#authorize
    @description Triggers the authorization process, gets the pin and token.
    ###
    authorizeStation = ->
      # gets the pin
      requestPIN().then (pin) ->
        # gets the token
        requestToken(stationPIN: pin).then (token) ->
          # success, resolve
          stationPIN: pin
          stationToken: token
        , (reason) ->
          # failed, is the token expired
          if reason.status is 404
            reset() # pin expired, reset station
            authorizeStation() # retry authorization
          else
            # other reason for failure
            $q.reject reason

    ###*
    @ngdoc method
    @name stationAuth#getToken
    @description Gets the station token.
    @returns {String} The token.
    ###
    getStationToken = -> $localStorage.stationAuth.token

    ###*
    @ngdoc method
    @name stationAuth#isAuthorized
    @description Tells if the station is authorized, aka there is a token.
    @returns {Boolean} Is authorized or not.
    ###
    isAuthorized = -> getStationToken()?

    # return service methods
    authorize: authorizeStation
    getToken: getStationToken
    isAuthorized: isAuthorized
    reset: reset

  return

