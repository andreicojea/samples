###*
@ngdoc directive
@name aspectRatioContainer
@module app
@restrict E

@description
A container which maintains a given aspect ratio.
To provide extended functionality, it actually consists out of two container, an outer and an inner container, each one of them with their own aspect ratio.

The role of the inner container is to fit the space inside the outer container, but maintaining it's own aspect ratio.
It behaves exactly like a backbround image with `background-size: contain` in regards to the way it fits the available space.
It also gets centered by default, but this is a matter of css and it can easily be overridden.

This directive has been designed to work along with the `playlistPlayer` and deal with portrait/landscape device types in different scenarios,
like the screen resolution, the change in size when switching orientation, regular or fullscreen preview, device border width, etc

@element aspect-ratio-container
@param {number=} ratio The outer container ratio. Can be a scope variable, a constant or anything which can be counted as an Expression and returns a number.
@param {number=} inner-ratio The inner container ratio. If not specified, the inner container will fill the whole space available in the outer container.

@example
```
aspect-ratio-container ratio="1" inner-ratio="previewPlayerAspectRatio"
```
###


angular.module "App"

.directive "aspectRatioContainer", [ ->
  restrict: "E"
  templateUrl: "<%= asset_path('application/directives/aspect_ratio_container.html') %>"
  transclude: true
  scope:
    ratio: "&?"
    innerRatio: "&?"
  link: (scope, elem, attrs) ->

    isAutoRatio = not attrs.ratio?
    innerContainer = elem.find ".aspect-ratio-inner-container"

    ###*
    @ngdoc method
    @name aspectRatioContainer#pxToNumber
    @description String-to-Number function for css `px` values.
    @param {string} s String containing a css `px` value
    @returns {number} The number representation of that string.
    @example
    ```
    pxToNumber("10px") == 10
    ```
    ###
    pxToNumber = (s) -> Number s.replace /px$/, ''

    ###*
    @ngdoc method
    @name aspectRatioContainer#autoRatio
    @description Computes the elem ratio, can be set by css.
    @returns {number} The elem ratio.
    ###
    autoRatio = -> elem.height() / elem.width()

    ###*
    @ngdoc method
    @name aspectRatioContainer#updateSizes
    @description Main update fn, computes and updates inner and outer container sizes.
    ###
    updateSizes = ->
      # evaluate inner and outer ratio attributes
      ratio = if isAutoRatio then autoRatio() else scope.ratio()
      innerRatio = scope.innerRatio()

      if ratio?
        # compute outer container height, depending on width and ratio
        elemWidth = elem.width()
        elem.height ratio * elemWidth unless isAutoRatio

        # reset inner ratio width and height.
        # initially they are empty strings, so unless a inner ratio value is provided
        # the inline `width` and `height` css props on the inner container will be removed
        innerElemHeight = ""
        innerElemWidth = ""

        if innerRatio?
          # take in consideration the inner container borders.
          # while computing the inner contaier size, apply the ratio on the
          # content area of the div, do not include the borders
          borderTop = pxToNumber innerContainer.css "borderTopWidth"
          borderBottom = pxToNumber innerContainer.css "borderBottomWidth"
          borderLeft = pxToNumber innerContainer.css "borderLeftWidth"
          borderRight = pxToNumber innerContainer.css "borderRightWidth"

          # compute available space inside the outer container
          # substract the borders of the inner container
          availableWidth = elem.innerWidth() - borderLeft - borderRight
          availableHeight = elem.innerHeight() - borderTop - borderBottom
          availableRatio = availableHeight / availableWidth

          if availableRatio < innerRatio # wider container
            innerElemHeight = availableHeight
            innerElemWidth = 1/innerRatio * innerElemHeight
          else
            innerElemWidth = availableWidth
            innerElemHeight = innerRatio * innerElemWidth

        # if `width` and `height` available, will be set as inline css,
        # otherwise, if the values are emty strings, will remove inline css
        innerContainer
          .height innerElemHeight
          .width innerElemWidth

    # update when values change.
    # notice the use of paranthesis (like function calls),
    # this is a watcher on expression values, so it evaluates those expressions
    scope.$watchGroup ["ratio()", "innerRatio()"], updateSizes

    # update on resize
    # notice the use of the jquery resize plugin, this is not the `resize` event on `window`,
    elem.resize updateSizes

    # destroy
    scope.$on "$destroy", ->
      # custom destroy of jquery resize plugin,
      # use this instead of `elem.unbind "resize"`
      elem.removeResize()
]