angular.module "App"

.controller "ArticleCtrl", [
  "$scope", "$stateParams", "ObjectProvider", "Utils", "$q"
  ($scope, $stateParams, ObjectProvider, Utils, $q) ->

    $scope.tabsId = $stateParams.tabs_id

    getArticle = ->
      ObjectProvider.get("articles", $stateParams.id)

    getParent = ->
      if $stateParams.parent?
        if $stateParams.parent is "fav"
          ObjectProvider.favourites().then (favourites) ->
            # this returns a collection-like object
            content_attributes: articles: favourites
        else
          ObjectProvider.get("nodes", $stateParams.parent)
      else
        $q.when null # returns resolved promise

    getRelated = ->
      relatedArticleIds = $scope.article.related_articles
      if relatedArticleIds?
        ObjectProvider.get("articles").then (allArticles) ->
          relatedArticles =  _.filter allArticles, (searchArticle) ->
            searchArticle?.id in relatedArticleIds
      else
        $q.when null # returns resolved promise

    getOther = ->
      switch $scope.articleParent?.content_type
        when "ArticleCollection"
          $q.when $scope.articleParent.content_attributes.articles
        when "Calendar"
          allEvents = $scope.articleParent.content_attributes.calendar_items_attributes
          allArticles = (calendarEvent.article for calendarEvent in allEvents)
          $q.when allArticles
        else $q.when []


    # get the article data
    getArticle().then (article) ->
      $scope.article = angular.copy article
      $scope.article.featured_image = Utils.determineMediumSize(article.featured_image_url)

      # get parent node
      getParent().then (parent) ->
        $scope.articleParent = parent

        # get other articles in collection/calendar
        getOther().then (articles) ->
          $scope.otherArticles = articles
          $scope.articleIndex = _.findIndex articles, {id: $scope.article.id}

          if $scope.articleParent?.content_type is "Calendar"
            allEvents = $scope.articleParent.content_attributes.calendar_items_attributes
            $scope.calendarEvent = allEvents[$scope.articleIndex]

        # get related articles
        getRelated().then (related) ->
          $scope.relatedArticles = related

    $scope.thumbUrl = Utils.getArticleThumbUrl
    $scope.formatDate = Utils.formatDate

    $scope.textScale = ->
      "#{0.5+$scope.current.textScale/200}em"

    $scope.gotoRelated = (article, animation = "forward") ->
      relatedUrl = $scope.articleUrl(article, $scope.tabsId)
      if $stateParams.parent? then relatedUrl += "?parent=#{$stateParams.parent}"
      $scope.goto relatedUrl, animation, {replace: true}

]
