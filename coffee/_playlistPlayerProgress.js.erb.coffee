###*
@ngdoc directive
@name playlistPlayerProgress
@module app
@restrict E
@description
This directive is the progress bar, or seek bar, for a {@link app.directive:playlistPlayer playlistPlayer}.
It is similar to a video seek bar and has the same kind of functionality, but for a whole set of media items
which represent a playlist. The `playlistPlayer` and this directive communicate through the `controls` object.

One interesting feature is that it can be restricted to a time interval, not necessarily the whole playlist.
This way, the red marker on the Playlist page only covers the timespan of the current slot.

## The `controls.currentTime`
The current time of a playlist gets updated from time to time, not continuously. For this reason, this directive
cannot use an existing slider implementation, which would make the seek handle jump from one position to another,
instead of animating. This directive animates it in anticipation, and it syncs up whenever the `currentTime`
gets updated, or when it changes the play/pause state.

The dragging functionality on the handler is implemented using jQueryUI's `draggable`.


@element playlist-player-progress
@param {object} controls Reference to a scope variable containing the current state of the playback.
See it explained in the {@link app.directive:playlistPlayer playlistPlayer} documentation.
@param {number=} minTime The timespan which this seek bar covers begins with this moment.
If not specified, it will be 0, the beginning of the playlist.
@param {number=} maxTime The timespan which this seek bar covers ends with this moment.
If not specified, it will be the playlist's end time.

@example
```
playlist-player playlist="playlistPreview" controls="previewControls" on-slot-change="updateSelectedTimelineSlot()"
playlist-player-progress controls="previewControls"
```
###
angular.module "App"

.directive "playlistPlayerProgress", [ ->
  restrict: "E"
  templateUrl: "<%= asset_path('application/directives/playlist_player_progress.html') %>"
  scope:
    controls: "="
    minTime: "&?"
    maxTime: "&?"
  link: (scope, elem, attrs) ->

    # get dom elems once
    trackElem = elem.find ".control-track"
    progElem = elem.find ".control-progress"
    handleElem = elem.find ".control-handle"

    # internal isSeeking
    # needed because multiple seekers can exist simultaneously
    thisIsSeeking = false

    ###*
    @ngdoc method
    @name playlistPlayer#getMinTime
    @description Gets the minTime from attribute or 0 if the attribute is missing.
    @returns {number} The minTime
    ###
    getMinTime = ->
      minTime = scope.minTime()
      if minTime? then minTime else 0

    ###*
    @ngdoc method
    @name playlistPlayer#getMaxTime
    @description Gets the maxTime from attribute or the playlist's end time if the attribute is missing.
    @returns {number} The maxTime
    ###
    getMaxTime = ->
      maxTime = scope.maxTime()
      if maxTime? then maxTime else scope.controls.totalDuration - 1

    ###*
    @ngdoc method
    @name playlistPlayer#stopAnimations
    @description Stops any current jQuery animation
    ###
    stopAnimations = ->
      progElem.stop(true)
      handleElem.stop(true)

    ###*
    @ngdoc method
    @name playlistPlayer#stopAnimations
    @description Main sync function, updates the state of the seek bar to reflect the state of the player.
    ###
    sync = ->
      if scope.controls?.currentTime? # is initialized?

        stopAnimations() # first stop current animations

        if not thisIsSeeking # user is not dragging the handle
          # compute the current progress
          progress = getProgress scope.controls.currentTime
          progressPercent = "#{progress * 100}%"

          # apply the css to reflect the current progress
          progElem.css width: progressPercent
          handleElem.css left: progressPercent

        # if the playback is running, animate the handle
        if scope.controls.isPlaying && !(scope.controls.isBuffering || scope.controls.isSeeking)
          timeLeft = Math.max 0, getMaxTime() - scope.controls.currentTime
          progElem.animate width: "100%", timeLeft, "linear"
          handleElem.animate left: "100%", timeLeft, "linear"

    ###*
    @ngdoc method
    @name playlistPlayer#getProgress
    @description This converts a time value into a percentage representing the progress
    @param {number} currentTime The current time of the playlist
    @returns {number} The progress, ranging from 0 to 1
    ###
    getProgress = (currentTime) ->
      minTime = getMinTime()
      maxTime = getMaxTime()
      if minTime < maxTime # avoid division by 0
        Math.max 0, Math.min 1, (currentTime - minTime) / (maxTime - minTime)
      else 0

    ###*
    @ngdoc method
    @name playlistPlayer#getProgress
    @description This converts a percentage representing the progress into a time value
    @param {number} progress The progress, ranging from 0 to 1
    @returns {number} The time which represents the given progress
    ###
    getCurrentTime = (progress) ->
      minTime = getMinTime()
      maxTime = getMaxTime()
      Math.max minTime, Math.min maxTime, Math.round minTime + progress * (maxTime - minTime)

    # sync when current time and limits change, also on play/pause
    scope.$watchGroup [
      "controls.currentTime",
      "minTime()", "maxTime()",
      "controls.totalDuration",
      "controls.isPlaying && !(controls.isBuffering || controls.isSeeking)"
    ], sync


    # avoid triggering digest cycles while dragging
    updateCurrentTime = _.throttle (currentTime) ->
      scope.$applyAsync -> scope.controls?.currentTime = currentTime
    , 250

    # make handler draggable using jQueryUI
    handleElem.draggable
      axis: "x"
      containment: "parent"
      start: ->
        # user started dragging, stop animations, update the isSeeking flag
        stopAnimations() # stop immediately
        scope.$applyAsync ->
          scope.controls.isSeeking = true
          thisIsSeeking = true
      drag: (ev, ui) ->
        # user is currently dragging, compute the progress, update currentTime
        trackWidth = trackElem.width()
        progElem.width ui.position.left
        progress = if trackWidth > 0 then ui.position.left / trackWidth else 0
        currentTime = getCurrentTime progress
        updateCurrentTime currentTime
      stop: ->
        # user stopped dragging, update isSeeking
        scope.$applyAsync ->
          scope.controls.isSeeking = false
          thisIsSeeking = false

    # set isSeeking true as soon as the handle gets clicked
    handleElem.on "mousedown touchstart", (ev) ->
      stopAnimations() # stop immediately
      scope.$applyAsync ->
        scope.controls.isSeeking = true
        thisIsSeeking = true
        stopAnimations()

    # set isSeeking false when the click or tap ends
    handleElem.on "mouseup touchend", (ev) ->
      scope.$applyAsync ->
        scope.controls.isSeeking = false
        thisIsSeeking = false

    # enable handler only when data is available
    scope.$watch "controls", (controls) ->
      enableAction = if controls? then "enable" else "disable"
      handleElem.draggable enableAction

    # make the track clickable, to update the progress
    trackElem.click (ev) ->
      # do it only if clicked on the track element, not any children
      if scope.controls? and this is ev.target
        offsetX = ev.offsetX or ev.clientX - $(ev.target).offset().left # firefox fix
        trackWidth = trackElem.width()
        progress = if trackWidth > 0 then offsetX / trackWidth else 0
        scope.$applyAsync -> # update the scope
          scope.controls.currentTime = getCurrentTime progress

    # destroy, remove draggable
    scope.$on "$destroy", ->
      handleElem.draggable "destroy"
]