angular.module "App"

.directive "panzoomGallery", ["$rootScope", "$animate", "$timeout", ($rootScope, $animate, $timeout) ->
  restrict: "CAE"
  templateUrl:  "views/tpl/panzoom_gallery.html"
  scope:
    currentSlide: "=slide"
    showCaption: "="
  link: (scope, elem, attr) ->
    imgElem = elem.children "img"
    imgNaturalHeight = null
    imgNaturalWidth = null
    imgHeight = null
    imgWidth = null
    elemHeight = null
    elemWidth = null

    scope.loading ?= true
    scope.showCaption ?= true

    resizeImageToFitContainer = ->
      elemHeight = elem.height()
      elemWidth = elem.width()
      elemRatio = elemHeight / elemWidth
      imgRatio = imgNaturalHeight / imgNaturalWidth

      imgHeight = elemHeight
      imgWidth = elemWidth
      paddingVertical = ""
      paddingHorizontal = ""

      if imgRatio > elemRatio # img has margin left/right
        imgWidth = elemHeight / imgNaturalHeight * imgNaturalWidth
        paddingHorizontal = (elemWidth - imgWidth) / 2
      else
        imgHeight = elemWidth / imgNaturalWidth * imgNaturalHeight
        paddingVertical = (elemHeight - imgHeight) / 2

      imgElem.css
        height: elemHeight
        width: elemWidth
        paddingTop: paddingVertical
        paddingBottom: paddingVertical
        paddingLeft: paddingHorizontal
        paddingRight: paddingHorizontal

    applyPanzoomOnImage = ->
      imgElem.panzoom duration: 150

    removePanzoomFromImage = ->
      imgElem.panzoom "destroy"

    getPanzoomMatrix = (panzoom) ->
      transform = panzoom.getTransform()
      if transform is "none" then ["1", "0", "0", "1", "0", "0"]
      else transform.split(/matrix\(|,\s*|\)/).slice(1, 7)

    getPanzoomState = (panzoom) ->
      matrix = getPanzoomMatrix panzoom
      # return
      zoom: Number matrix[0]
      panX: Number matrix[4]
      panY: Number matrix[5]
      time: new Date().getTime()

    isTapEvent = (startState, endState) ->
      if startState? and endState?
        noZoomChanged = startState.zoom is endState.zoom
        timeIsSmall = endState.time - startState.time < 400 # less than 400ms
        panXIsSmall = Math.abs(startState.panX - endState.panX) < 10
        panYIsSmall = Math.abs(startState.panY - endState.panY) < 10
        # return
        noZoomChanged and timeIsSmall and panXIsSmall and panYIsSmall

    removeUnnecesaryPan = (panzoom, matrixState) ->
      clientRect = imgElem[0].getBoundingClientRect()

      marginLeft = clientRect.left
      marginRight = elemWidth - clientRect.right
      marginTop = clientRect.top
      marginBottom = elemHeight - clientRect.bottom

      moveX = 0
      moveY = 0

      if clientRect.width < elemWidth
        moveX = (marginRight - marginLeft) / 2
      else if marginLeft > 0
        moveX = -marginLeft
      else if marginRight > 0
        moveX = marginRight

      if clientRect.height < elemHeight
        moveY = (marginBottom - marginTop) / 2
      else if marginTop > 0
        moveY = -marginTop
      else if marginBottom > 0
        moveY = marginBottom

      panzoom.pan moveX, moveY,
        animate: true
        relative: true

    # ------------------
    # load new image
    # ------------------

    imgElem.on "load", ->
      imgNaturalHeight = this.naturalHeight
      imgNaturalWidth = this.naturalWidth
      resizeImageToFitContainer()
      applyPanzoomOnImage()

      scope.$applyAsync -> scope.loading = false

    # ------------------
    # panzoom events
    # ------------------

    startMatrixState = null
    endMatrixState = null

    imgElem.on "panzoomstart", (e, panzoom, ev, touches) ->
      startMatrixState = getPanzoomState panzoom
      if touches?.length is 2
        scope.$applyAsync -> scope.showCaption = false

    imgElem.on "panzoomend", (e, panzoom, matrix, changed) ->
      endMatrixState = getPanzoomState panzoom
      # reset if image is zoomed out
      if endMatrixState.zoom < 1 then panzoom.reset()
      # reset on tap
      else if isTapEvent startMatrixState, endMatrixState then panzoom.reset()
      # center image if pan is large
      else removeUnnecesaryPan panzoom, endMatrixState

    imgElem.on "panzoomreset", (e, panzoom, matrix) ->
      scope.$applyAsync ->
        if isTapEvent startMatrixState, endMatrixState
          scope.showCaption = not scope.showCaption
        else
          scope.showCaption = true
        startMatrixState = null
        endMatrixState = null

    # ------------------
    # window resize
    # ------------------

    windowResizeCallback = ->
      resizeImageToFitContainer()
      imgElem.panzoom "resetDimensions"
      imgElem.panzoom "reset", animate: false

    $(window).on "resize", windowResizeCallback
    scope.$on "$destroy", ->
      $(window).off "resize", windowResizeCallback

]