$(window).load(function(){

  // --------------------------
  // SHOW OLD BROWSER MESSAGE
  // --------------------------

  $.reject({
    reject: {
        safari: 4, // Apple Safari
        chrome: 4, // Google Chrome
        firefox: 4, // Mozilla Firefox
        msie: 8, // Microsoft Internet Explorer
        opera: 10 // Opera
    },
    overlayOpacity: 1,
    close: false,
    imagePath: "bower_components/TurnWheel/jReject/images/"
  });

  // --------------------------
  // INITIALIZE ANIMATION
  // --------------------------

  // get the json data first
  $.getJSON(cadolines.json, function(data) {

    // init animation
    var canvas = $("#cadolines-canvas").get(0);
    var animation = new cadolines.Animation(canvas, data);

    // show canvas when it's ready, wait 2 frames
    var counter = 0;

    function init() {
      counter += 1;
      if(counter > 2) {
        paper.view.off("frame", init)
        $("#cadolines-canvas").removeClass("loading");
        $("#mobile-intro").removeClass("loading");
      }
    }
    paper.view.on("frame", init);

  });
});



$(window).ready(function(){

  // --------------------------
  // MOBILE INTRO
  // --------------------------

  var container = $("#mobile-intro");

  // container resize
  function containerResize() {
    container.width(window.innerWidth);
    container.height(window.innerHeight);
  }

  // initial resize
  containerResize();

  // update on window resize
  $(window).on("resize", function(){
    // only if the window width have changed
    if (window.innerWidth != container.width()) {
      containerResize();
    };
  });

  // --------------------------
  // VIDEO MODAL
  // --------------------------

  // remove src from iframe to prevent video playing
  // but save it first for later use
  $("#video-modal iframe").each(function(){
    $(this).data("src", $(this).attr("src"));
    $(this).attr("src", "");
  });

  // open the modal when clicking this button
  $("#open-video-modal-btn").click(function() {
    $(this).modal({
      fadeDuration: 250
    });
    return false;
  });

  // before opening the modal, resize and make visible
  $("#video-modal").on('modal:before-open', function() {

    // 'initial' class only hides the modal initially
    $(this).removeClass("initial");

    // fix jquery-modal bug, not showing the modal
    // after previously opening and closing it
    $(this).show();

    // make the iframe 80% the size of the window
    $(this).find("iframe").each(function() {
      var maxWidth = 2 * 500;
      var maxHeight = 2 * 281;

      var windowWidth = $(window).width();
      var windowHeight = $(window).height();
      var videoWidth = $(this).attr("width") || maxWidth;
      var videoHeight = $(this).attr("height") || maxHeight;

      var windowRatio = windowHeight / windowWidth;
      var videoRatio = videoHeight / videoWidth;

      var newWidth = 0.8 * windowWidth;
      var newHeight = 0.8 * windowHeight;

      if (windowRatio > videoRatio) { // tall window
        newHeight = newWidth * videoRatio;
      } else { // tall video
        newWidth = newHeight / videoRatio;
      };

      $(this).css({
        width: Math.min(newWidth, maxWidth),
        height: Math.min(newHeight, maxHeight)
      });

      // add src back, will start playing the video
      $(this).attr("src", $(this).data("src"));
    });
  })

  // on modal close, remove the src again to stop the video
  $("#video-modal").on('modal:after-close', function() {
    $(this).find("iframe").attr("src", "");
  })

  // on canvas resize, update the modal trigger position and size
  $("#cadolines-canvas").on("animationscale", function(){
    var scale = $(this).attr("data-animation-scale");
    var right = Number($(this).attr("data-animation-position-x")) + 15;
    $("#open-video-modal-btn").css({
      width: scale * 350,
      right: right
    });
  });

  // --------------------------
  // GO TOP - ANIMATED
  // --------------------------

  $("#go-top-btn").on("touchend", function() {
    var canvas = $("#cadolines-canvas")[0];

    var bodyRect = document.body.getBoundingClientRect(),
        elemRect = canvas.getBoundingClientRect(),
        offset   = elemRect.top - bodyRect.top;

    var body = $("html, body");
    body.stop().animate({scrollTop: offset});
  });

});