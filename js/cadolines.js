"use strict";

// ===============================================================
// CADOLINES NAMESPACE
// - create a namespace to avoid poluting the global namespace
// - this includes a configuration object which contains animation
//   params, object locations, sizes, colors, etc
// - exposes some helper methods
// ===============================================================

var cadolines = (function() {

  var instance = {
    cache: {}
  };

  // --------------------------------------------------------------
  // CONFIG
  // - this object contains the animation params for both mobile & desktop
  // - on resize, the `current` object will contain the config values
  //   specific to the current window size
  // - the `current` config will not contain the `desktop` and `mobile` keys
  // --------------------------------------------------------------

  instance.config = {
    breakpoint: 768,
    size: {
      desktop: new paper.Size(1920, 1080),
      mobile: new paper.Size(505, 1400)
    },
    parallaxFactor: {
      desktop: -0.015,
      mobile: 0
    },
    groups: {
      hexagonSize: 180/215 * 215,
      hexagonTextSize: 180/215 * 46,
      hexagonFont: "Kabel",
      triangleFont: "Lato",
      trianglesGap: 8,
      idleOpacity: 1,
      blurOpacity: 0.2,
      idleTextColor: new paper.Color("#fff"),
      focusTextColor: new paper.Color("#fff"),
      idlePositions: {
        desktop: [
          new paper.Point(345, 560),
          new paper.Point(940, 700),
          new paper.Point(1550, 630)
        ],
        mobile: [
          new paper.Point(200, 250),
          new paper.Point(300, 700 - 40),
          new paper.Point(200, 1150 - 80),
        ]
      },
      focusScale: 260/180,
      focusPosition: {
        desktop: new paper.Point(570, 690),
        mobile: new paper.Point(250, 300)
      },
      shadowColor: "rgba(0,0,0,0.2)",
      focusShadowOffset: {
        desktop: new paper.Point(50, 80),
        mobile: new paper.Point(100, 100)
      },
      idleShadowOffsets: {
        desktop: [
          new paper.Point(34, 62),
          new paper.Point(55, 100),
          new paper.Point(110, 170)
        ],
        mobile: [
          new paper.Point(0.5 * 34, 0.5 * 62),
          new paper.Point(0.5 * 55, 0.5 * 100),
          new paper.Point(0.5 * 110, 0.5 * 170)
        ]
      }
    },
    close: {
      position: {
        desktop: new paper.Point(1630, 920),
        mobile: new paper.Point(410, 50)
      }
    }

  };

  // tells if an object is a plain object
  function isPlainObject(o) {
    return typeof o == 'object' && o.constructor == Object;
  };

  // tells if it's mobile window size
  function isMobile() {
    return window.innerWidth < instance.config.breakpoint
  }

  // returns the config values for a given platform (mobile/desktop)
  function getCurrentConfig(config, platform){
    config = config[platform] || config;

    if (isPlainObject(config)) {
      var current = {};
      for(var key in config){
        current[key] = getCurrentConfig(config[key], platform);
      }
      return current;
    } else {
      return config;
    }
  };

  // returns useful info about the triangles.
  // knowing the hexagon size, it returns:
  // - the triangle size
  // - the distance between the triagle center and the hexagon center
  // - the distance between triangles
  function getTrianglesHelper(config) {
    var gap = config.groups.trianglesGap;

    var hexagonOuterRadius = config.groups.hexagonSize;
    var hexagonInnerRadius = hexagonOuterRadius * Math.sqrt(3) / 2;
    var largeTriangleOuterRadius = 2/3 * hexagonInnerRadius;
    var largeTriangleInnerRadius = largeTriangleOuterRadius / 2;
    var smallTriangleOuterRadius = largeTriangleOuterRadius - 2/3 * gap;

    var distanceFromHexagonCenter = hexagonInnerRadius + largeTriangleInnerRadius + 2/3 * gap;
    var distanceBetweenTriangles = smallTriangleOuterRadius + gap;

    return {
      triangleSize: smallTriangleOuterRadius,
      distanceFromHexagonCenter: distanceFromHexagonCenter,
      distanceBetweenTriangles: distanceBetweenTriangles
    }
  }

  // returns the triangle positions/rotations for desktop
  function getDesktopTriangles(config) {
    var helper = getTrianglesHelper(config);
    var firstTrianglePosition = new paper.Point(0, -helper.distanceFromHexagonCenter).rotate(30);
    var nextRelativePosition = new paper.Point(helper.distanceBetweenTriangles, 0);

    // calculate positions
    var rotateRelativePosition = [null, 0, 60, 120, 60, 0, -60, -120, -60, 0, 60, 120];
    var trianglePositions = [firstTrianglePosition];

    for (var i = 1; i < 12; i++) {
      var prevPosition = trianglePositions[i - 1];
      var relativePosition = nextRelativePosition.rotate(rotateRelativePosition[i]);
      trianglePositions[i] = prevPosition.add(relativePosition);
    };

    // calculate rotations
    var triangleRotations = [];
    for (var i = 0; i < 12; i++) {
      triangleRotations[i] = Math.pow(-1, i) * 30;
    };

    return {
      size: helper.triangleSize,
      positions: trianglePositions,
      rotations: triangleRotations
    }
  }

  // returns the triangle positions/rotations for mobile
  function getMobileTriangles(config) {
    var helper = getTrianglesHelper(config);
    var firstTrianglePosition = new paper.Point(0, -helper.distanceFromHexagonCenter).rotate(180 - 30);
    var nextRelativePosition = new paper.Point(helper.distanceBetweenTriangles, 0);

    // calculate positions
    var trianglePositions = [
      firstTrianglePosition
    ];

    for (var i = 1; i < 4; i++) {
      var prevPosition = trianglePositions[i - 1];
      var relativePosition = nextRelativePosition.rotate((i+1)*60);
      trianglePositions[i] = prevPosition.add(relativePosition);
    };
    for (var i = 4; i < 12; i++) {
      var prevPosition = trianglePositions[i - 4];
      var relativePosition = nextRelativePosition.rotate(120).add(nextRelativePosition.rotate(60));
      trianglePositions[i] = prevPosition.add(relativePosition);
    };

    // calculate rotations
    var triangleRotations = [];
    for (var i = 0; i < 12; i++) {
      triangleRotations[i] = Math.pow(-1, i) * 30;
    };

    return {
      size: helper.triangleSize,
      positions: trianglePositions,
      rotations: triangleRotations
    }
  }

  // returns the current triangle positions/rotations
  function getCurrentTriangles(config) {
    if(config.platform == "desktop") {
      return getDesktopTriangles(config)
    } else {
      return getMobileTriangles(config)
    }
  }

  // update the current config, populate `current` object
  function updateCurrentConfig() {
    var platform;
    if(isMobile()) {
      platform = "mobile";
    } else {
      platform = "desktop";
    }

    if(!instance.cache[platform]) {
      // no cache? calculate the config (only once needed)
      var platformConfig = getCurrentConfig(instance.config, platform);
      platformConfig.platform = platform;
      platformConfig.triangles = getCurrentTriangles(platformConfig);

      // cache this
      instance.cache[platform] = platformConfig;
    }

    instance.current = instance.cache[platform];
  }

  updateCurrentConfig() // initial config
  window.addEventListener("resize", updateCurrentConfig, true); // update on resize


  // --------------------
  // BROWSER/DEVICE INFO
  // --------------------

  function detectSafari() {
    return navigator.vendor && navigator.vendor.indexOf('Apple') > -1 && navigator.userAgent && !navigator.userAgent.match('CriOS');
  }

  function detectIE() {
    var ua = window.navigator.userAgent;

    // test values
    // IE 10
    //ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';
    // IE 11
    //ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';
    // IE 12 / Spartan
    //ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf('rv:');
      return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
      // IE 12 => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
  }

  // this affects shadow rendering
  instance.devicePixelRatio = window.devicePixelRatio || 1;

  instance.isSafari = detectSafari();
  instance.isIE = detectIE();



  // -------------
  // UTILS
  // -------------

  // expose isMobile
  instance.isMobile = isMobile;

  // linear interpolation for vectors
  // http://gamedev.stackexchange.com/questions/18615/how-do-i-linearly-interpolate-between-two-vectors
  instance.interpolatePoints = function(A, B, t) {
    return A.multiply(1.0-t).add(B.multiply(t));
  }

  // linear interpolation for values
  instance.interpolateValues = function(A, B, t) {
    return A + (B - A) * t;
  }

  // linear interpolation for colors
  instance.interpolateColors = function(A, B, t) {
    return A.multiply(1.0-t).add(B.multiply(t));
  }

  // rescale taking in consideration previous scale
  instance.rescale = function(style, paperObject, newScale, scaleKey) {
    scaleKey = scaleKey || "scale";

    var relativeScale = 1 / style[scaleKey] * newScale;
    style[scaleKey] = newScale;
    paperObject.scale(relativeScale);
  }

  // custom animations
  instance.animate = function(obj, name, opts) {
    opts = opts || {};

    // default options
    var easing = opts.easing || TWEEN.Easing.Quadratic.InOut;
    var duration = opts.duration || 1000;
    var delay = opts.delay || 0;

    var from = {
      t: 0,
      val: opts.from
    }
    var to = {
      t: 1,
      val: opts.to
    }

    var noopFn = function(){};
    var onUpdate = opts.onUpdate || noopFn;
    var onComplete = opts.onComplete || noopFn;

    // setup animation queue
    obj.tweens = obj.tweens || {};

    // any previous animation?
    if(obj.tweens[name]) {
      // resume?
      if(opts.resume) {
        from = obj.tweens[name].current || to;
        duration = from.t * duration;
      }

      // cancel previous tween
      obj.tweens[name].stop();
      delete obj.tweens[name];
    }

    // if mobile, don't animate
    if (instance.isMobile()) {
      onUpdate.call(obj, to.val);
      onComplete.call(obj);
    } else { // if desktop, animate

      // create a new tween
      obj.tweens[name] = new TWEEN.Tween(from)
        .to(to, duration)
        .easing(easing)
        .onUpdate(function(){
          obj.tweens[name].current = this;
          onUpdate.call(obj, this.val);
        })
        .onComplete(function(){
          delete obj.tweens[name];
          onComplete.call(obj);
        })
        .delay(delay)
        .start();
    };

  }

  // set a mouse down point on a clickable element
  // used for discriminating between drag/click events
  instance.mouseDown = function(ev) {
    this.mouseDownPoint = ev.point.subtract(new paper.Point(0, $(window).scrollTop()));
  }

  // set a mouse down point on a clickable element
  // used for discriminating between drag/click events
  instance.mouseUp = function(ev, clickCallback) {
    if(this.mouseDownPoint) {
      var mouseUpPoint = ev.point.subtract(new paper.Point(0, $(window).scrollTop()));
      var dragMove = mouseUpPoint.subtract(this.mouseDownPoint).length;
      if (dragMove < 10) {
        clickCallback();
      };
    }

    this.mouseDownPoint = null;
  }

  // simple throttle implementation, used for parallax
  instance.throttle = function(fn, threshhold, scope) {
    threshhold || (threshhold = 250);
    var last,
        deferTimer;
    return function () {
      var context = scope || this;

      var now = +new Date,
          args = arguments;
      if (last && now < last + threshhold) {
        // hold on to it
        clearTimeout(deferTimer);
        deferTimer = setTimeout(function () {
          last = now;
          fn.apply(context, args);
        }, threshhold);
      } else {
        last = now;
        fn.apply(context, args);
      }
    };
  }

  // fire an event when the canvas gets resized
  // to allow other scripts to use the scale and position
  instance.triggerScaleEvent = function(element, style) {
    element.setAttribute("data-animation-scale", style.scale);
    element.setAttribute("data-animation-position-x", style.position.x);
    element.setAttribute("data-animation-position-y", style.position.y);

    var event; // The custom event that will be created

    if (document.createEvent) {
      event = document.createEvent("HTMLEvents");
      event.initEvent("animationscale", true, true);
    } else {
      event = document.createEventObject();
      event.eventType = "animationscale";
    }

    event.eventName = "animationscale";

    if (document.createEvent) {
      element.dispatchEvent(event);
    } else {
      element.fireEvent("on" + event.eventType, event);
    }
  }

  return instance;
})();


// ===============================================================
// ANIMATION CLASS
// - initializes paperjs, creates groups, logo, background, etc
// - responsible for scaling and centering the entire scene
// - sets up parallax (if not disabled)
// ===============================================================

cadolines.Animation = function(canvas, data) {
  var thisAnimation = this;
  this.style = {};

  // parallax
  this.parallaxCallbacks = [];
  this.parallaxMove = new paper.Point(0, 0);

  // setup paper + canvas
  this.setupPaper(canvas);

  // scale before creating children
  this.autoscale()

  // rescale on window resize
  window.addEventListener("resize", function() {
    thisAnimation.autoscale();
  }, true);

  // add logo, background, close btn
  this.setupDecorative();

  // add groups
  this.setupGroups(data.groups);

  // add parallax
  // this.setupParallax();
}

// applies paperjs and enables tweenjs animations
cadolines.Animation.prototype.setupPaper = function(canvas) {
  this.canvas = canvas;

  // setup paperjs project
  paper.setup(this.canvas);

  // update tween.js on every frame
  paper.view.on("frame", function () {
    TWEEN.update();
  });

  // create a paper group for everything
  this.paperGroup = new paper.Group();
}

// sets up logo, light and background
cadolines.Animation.prototype.setupDecorative = function() {
  // create objects
  this.logo = new cadolines.Logo(this);
  this.background = new cadolines.Background(this);
  this.closeButton = new cadolines.CloseButton(this);

  // initial states
  this.closeButton.setOpenAt(0);
  this.background.lightCircle.sendToBack();
  this.background.paperGroup.sendToBack();

  // add all to the paper group
  this.paperGroup.addChild(this.background.paperGroup);
  this.paperGroup.addChild(this.background.lightCircle);
  this.paperGroup.addChild(this.logo.paperItem);
  this.paperGroup.addChild(this.closeButton.paperGroup);
}

// create groups (group = hexagon + triangles)
cadolines.Animation.prototype.setupGroups = function(groupsData) {
  this.groups = [];

  for (var i = 0; i < groupsData.length; i++) {
    // create a group object
    var group = new cadolines.Group(groupsData[i], this, i);
    // add to groups
    this.groups.push(group);
    // initial state
    group.setFocusAt(0);
    // add to global paper group
    this.paperGroup.addChild(group.paperGroup);
  }
}

// sets up the parallax effect
cadolines.Animation.prototype.setupParallax = function() {
  var self = this;

  var throttleTriggerParallax = cadolines.throttle(this.triggerParallax, 50, this);

  this.paperGroup.on("mousemove", function(ev){
    var fromCenter = ev.point.subtract(paper.view.bounds.center);
    self.parallaxMove = fromCenter.multiply(cadolines.current.parallaxFactor)
    throttleTriggerParallax();
  });
}

// create a parallax event listener
cadolines.Animation.prototype.onParallax = function(fn) {
  this.parallaxCallbacks.push(fn);
}

// run all parallax event listeners
cadolines.Animation.prototype.triggerParallax = function(fn) {
  for (var i = 0; i < this.parallaxCallbacks.length; i++) {
    this.parallaxCallbacks[i]();
  };
}

// scale, center, update view size, trigger scale event
cadolines.Animation.prototype.autoscale = function() {
  if(cadolines.isMobile()) {
    this.autoscaleMobile();
  } else {
    this.autoscaleDesktop();
  }

  // resize paper
  paper.view.viewSize = new paper.Size(this.style.canvasWidth, this.style.canvasHeight);

  // trigger scale event
  cadolines.triggerScaleEvent(this.canvas, this.style);
}

// scale and resize canvas on mobile
cadolines.Animation.prototype.autoscaleMobile = function() {
  // set position & scale
  this.style.position = new paper.Point(0, 0);
  this.style.scale = window.innerWidth / cadolines.current.size.width;

  // resize canvas elem
  this.style.canvasWidth = window.innerWidth;
  this.style.canvasHeight = this.style.scale * cadolines.current.size.height;
}

// scale and center on desktop
cadolines.Animation.prototype.autoscaleDesktop = function() {
  // calculate ratios, will be compared
  var animationRatio = cadolines.current.size.height / cadolines.current.size.width;
  var windowRatio = window.innerHeight / window.innerWidth;

  // initial values, some will be updated
  var animationWidth = window.innerWidth;
  var animationHeight = window.innerHeight;
  var animationScale;

  if(windowRatio > animationRatio) {
    // scale horizontally, center vertically
    animationHeight = animationRatio * animationWidth;
    animationScale = animationWidth / cadolines.current.size.width;
  } else {
    // scale vertically, center horizontally
    animationWidth = animationHeight / animationRatio;
    animationScale = animationHeight / cadolines.current.size.height;
  }

  // calculate position
  var animationX = (window.innerWidth - animationWidth) / 2;
  var animationY = (window.innerHeight - animationHeight) / 2;

  // set position & scale
  this.style.position = new paper.Point(animationX, animationY);
  this.style.scale = animationScale;

  // resize canvas elem
  this.style.canvasWidth = window.innerWidth;
  this.style.canvasHeight = window.innerHeight;
}

// given a relative point, this applies the global scale/position
cadolines.Animation.prototype.getRelativePoint = function(point) {
  return point.multiply(this.style.scale).add(this.style.position);
}

// given a relative scale (or length), this applies the global scale
cadolines.Animation.prototype.getRelativeScale = function(scale) {
  return this.style.scale * scale;
}

// focus a group => focusIn animation on that group, blur animation on others
cadolines.Animation.prototype.focusGroup = function(focusGroup) {
  // bring the group and the X button to front
  focusGroup.paperGroup.bringToFront();
  this.closeButton.paperGroup.bringToFront();

  // animate
  this.focusedGroup = focusGroup;
  focusGroup.animateFocusIn();
  this.closeButton.animateEnter();
  for (var i = 0; i < focusGroup.index; i++) {
    // true = move right
    this.groups[i].animateBlurIn(true);
  }
  for (var i = focusGroup.index + 1; i < this.groups.length; i++) {
    // false = don't move right
    this.groups[i].animateBlurIn(false);
  }

  // go top (mobile)
  if (cadolines.isMobile()) {
    var bodyRect = document.body.getBoundingClientRect(),
        elemRect = this.canvas.getBoundingClientRect(),
        offset   = elemRect.top - bodyRect.top;

    console.log(offset);
    window.scrollTo(0, offset);
  };
}

// close focused group
cadolines.Animation.prototype.closeFocusedGroup = function() {
  this.focusedGroup.animateFocusOut();
  this.closeButton.animateLeave();
  for (var i = 0; i < this.focusedGroup.index; i++) {
    // true = was moved right
    this.groups[i].animateBlurOut(true);
  }
  for (var i = this.focusedGroup.index + 1; i < this.groups.length; i++) {
    // false = wasn't moved right
    this.groups[i].animateBlurOut(false);
  }
  this.focusedGroup = null;
}


// ===============================================================
// GROUP CLASS
// - initializes the hexagon and triangles
// - has focus/blur animations
// - gets focused/closed on click
// ===============================================================

cadolines.Group = function(data, animation, index) {
  var self = this;
  this.animation = animation; // parent animation
  this.index = index; // which groups is this?
  this.style = {scale: 1}; // relative scale

  // create a paper group to contain the hexagon + triangles
  this.paperGroup = new paper.Group();
  this.paperGroup.pivot = new paper.Point(0, 0);

  // create the hexagon
  this.hexagon = new cadolines.Hexagon(data.hexagon, this);
  this.paperGroup.addChild(this.hexagon.paperGroup);

  // create triangles
  this.triangles = [];
  for (var i = 0; i < data.triangles.length; i++) {
    var triangle = new cadolines.Triangle(data.triangles[i], this, i);
    this.triangles.push(triangle);
    this.paperGroup.addChild(triangle.paperGroup);
    this.paperGroup.addChild(triangle.hoverTrigger);
    this.paperGroup.addChild(triangle.triangleShadow);
  }

  // init the triangles only after they are all created
  for (var i = 0; i < data.triangles.length; i++) {
    this.triangles[i].init();
  };

  // // hexagon click -> focus or navigate
  // this.hexagon.paperGroup.on("click", function(){
  //   if(self.animation.focusedGroup) {
  //     // is this focused? if focused and not animating, navigate
  //     if(self.animation.focusedGroup == self && !self.tweens.focus) {
  //       window.open(data.hexagon.link, "_blank");
  //     }
  //     // otherwise it's blurred, do nothing
  //   } else {
  //     self.animation.focusGroup(self);
  //   }
  // });

  // hexagon click -> focus or close
  function hexagonClick() {
    if(self.animation.focusedGroup) {
      // is this focused? unfocus on click
      if(self.animation.focusedGroup == self) {
        self.animation.closeFocusedGroup();
      }
      // otherwise it's blurred, do nothing
    } else {
      self.animation.focusGroup(self);
    }
  }

  // workaround for mobile devices:
  // don't use click event, it triggers when scrolling...
  this.hexagon.paperGroup.on("mousedown", cadolines.mouseDown);

  this.hexagon.paperGroup.on("mouseup", function(ev){
    cadolines.mouseUp.call(this, ev, hexagonClick);
  });

  // update on window resize
  paper.view.on("resize", function(){
    self.updatePositionScale();
  });

  // parallax
  this.animation.onParallax(function(){
    // do this only if animation is not in progress
    if (!self.tweens || !self.tweens.focus) {
      self.updatePositionScale();
    };
  });

}

// updates the scale and position of the current state (on resize)
cadolines.Group.prototype.updatePositionScale = function() {
  if (this.isFocusedAt < 0) {
    this.setBlurAt(-this.isFocusedAt, this.isMovedRight);
  } else {
    this.setFocusAt(this.isFocusedAt)
  };

  for (var i = 0; i < this.triangles.length; i++) {
    this.triangles[i].resetPosition();
  };
}

// focus in animation
cadolines.Group.prototype.animateFocusIn = function() {
  var hexagonDuration = 1200;
  var triangleDuration = 250;
  var trianglesDelay = 600;

  // animate group position
  cadolines.animate(this, "focus", {
    from: 0,
    to: 1,
    duration: hexagonDuration,
    onUpdate: this.setFocusAt,
    easing: TWEEN.Easing.Cubic.Out,
    resume: true
  });

  // animate triangles
  cadolines.animate(this, "triangles", {
    from: 0,
    to: this.triangles.length,
    duration: this.triangles.length * triangleDuration,
    delay: trianglesDelay,
    onUpdate: this.openTrianglesAt,
    easing: TWEEN.Easing.Linear.None,
    resume: true
  });
}

// focus out animation
cadolines.Group.prototype.animateFocusOut = function() {
  var hexagonDuration = 1200;
  var triangleDuration = 100;

  // animate group position
  cadolines.animate(this, "focus", {
    from: 1,
    to: 0,
    duration: hexagonDuration,
    delay: 0,
    onUpdate: this.setFocusAt,
    easing: TWEEN.Easing.Cubic.InOut,
    resume: true
  });

  // animate triangles
  cadolines.animate(this, "triangles", {
    from: this.triangles.length,
    to: 0,
    duration: this.triangles.length * triangleDuration,
    onUpdate: this.openTrianglesAt,
    easing: TWEEN.Easing.Cubic.InOut,
    resume: true
  });

}

// progress from 0.0 to 12.0,
// the floating point is the last triangle opacity
// 0 = no triangles open, 12 = all triangles are in
// 2.5 = 2 triangles fully visible, 3rd has 0.5 opacity
cadolines.Group.prototype.openTrianglesAt = function(progress) {
  var openedTriangles = Math.floor(progress);
  var currentProgress = progress - openedTriangles;

  for (var i = 0; i < openedTriangles; i++) {
    this.triangles[i].setOpenAt(1);
  };
  if (openedTriangles < this.triangles.length) {
    this.triangles[openedTriangles].setOpenAt(currentProgress);
  }
  for (var i = openedTriangles + 1; i < this.triangles.length; i++) {
    this.triangles[i].setOpenAt(0);
  };
}

// entering blur state
cadolines.Group.prototype.animateBlurIn = function(moveRight) {
  var hexagonDuration = 1200;

  // animate group position
  cadolines.animate(this, "blur", {
    from: 0,
    to: 1,
    duration: hexagonDuration,
    onUpdate: function(t){
      this.setBlurAt(t, moveRight);
    },
    easing: TWEEN.Easing.Cubic.Out,
    resume: true
  });
}

// exiting blur state
cadolines.Group.prototype.animateBlurOut = function(moveRight) {
  var hexagonDuration = 1200;

  // animate group position
  cadolines.animate(this, "blur", {
    from: 1,
    to: 0,
    duration: hexagonDuration,
    onUpdate: function(t){
      this.setBlurAt(t, moveRight);
    },
    easing: TWEEN.Easing.Circular.InOut,
    resume: true
  });
}

// set focusing animation progress
cadolines.Group.prototype.setFocusAt = function(progress) {
  // remember state
  this.isFocusedAt = progress;

  // calculate position
  var idlePosition = this.getIdlePosition();
  var focusPosition = this.getFocusPosition();
  var parallaxMove = this.animation.parallaxMove.multiply(Math.pow(this.style.scale, 3));
  var interpolatedPosition = cadolines.interpolatePoints(idlePosition, focusPosition, progress);
  var currentPosition = interpolatedPosition.add(parallaxMove);

  // set position
  this.paperGroup.position = currentPosition;

  // calculate scale
  var idleScale = this.getIdleScale();
  var focusScale = this.getFocusScale();
  var currentScale = cadolines.interpolateValues(idleScale, focusScale, progress);

  // set scale
  this.rescale(currentScale);

  // update shadows
  this.updateShadows(progress);

  // update children
  this.hexagon.setFocusAt(progress);

}

// set blurring animation progress
cadolines.Group.prototype.setBlurAt = function(progress, moveRight) {
  // remember state
  this.isFocusedAt = -progress;
  this.isMovedRight = moveRight;

  var interpolatedPosition;
  var parallaxMove = this.animation.parallaxMove.multiply(this.style.scale);

  if(moveRight) {
    // calculate position
    var idlePosition = this.getIdlePosition();
    var blurPosition = this.getMovePosition();
    var currentPosition = cadolines.interpolatePoints(idlePosition, blurPosition, progress);

    // set position
    interpolatedPosition = currentPosition;
  } else {
    interpolatedPosition = this.getIdlePosition();
  }

  var currentPosition = interpolatedPosition.add(parallaxMove);

  // set position
  this.paperGroup.position = currentPosition;

  // keep scale to 1
  this.rescale(this.animation.getRelativeScale(1));

  // update children
  this.hexagon.setBlurAt(progress);
}

// get idle position of this group
cadolines.Group.prototype.getIdlePosition = function() {
  var position = cadolines.current.groups.idlePositions[this.index];
  return this.animation.getRelativePoint(position);
}

// get focus position
cadolines.Group.prototype.getFocusPosition = function() {
  var position = cadolines.current.groups.focusPosition;
  return this.animation.getRelativePoint(position);
}

// get move position
cadolines.Group.prototype.getMovePosition = function() {
  if(this.index == this.animation.groups.length - 1) {
    return this.getIdlePosition();
  } else {
    var nextGroup = this.animation.groups[this.index + 1];
    return nextGroup.getIdlePosition();
  }
}

// get idle scale in context (applies global scale)
cadolines.Group.prototype.getIdleScale = function() {
  return this.animation.getRelativeScale(1);
}

// get focus scale in context (applies global scale)
cadolines.Group.prototype.getFocusScale = function() {
  return this.animation.getRelativeScale(cadolines.current.groups.focusScale);
}

// changes the scale taking into consideration the previous scale
cadolines.Group.prototype.rescale = function(newScale) {
  cadolines.rescale(this.style, this.paperGroup, newScale);
}

// update shadow offset and blur
cadolines.Group.prototype.updateShadows = function(progress) {
  var idleOffset = cadolines.current.groups.idleShadowOffsets[this.index];
  var focusOffset = cadolines.current.groups.focusShadowOffset;
  var interpolatedOffset = cadolines.interpolatePoints(idleOffset, focusOffset, progress);
  var shadowOffset = interpolatedOffset.multiply(this.animation.style.scale).multiply(cadolines.devicePixelRatio);
  var shadowBlur = shadowOffset.length * 3/4;

  this.hexagon.setShadow(shadowOffset, shadowBlur);
  for (var i = 0; i < this.triangles.length; i++) {
    this.triangles[i].setShadow(shadowOffset, shadowBlur);
  };
}


// ===============================================================
// HEXAGON CLASS
// - initializes the hexagon shape, decorations and text
// - hexagon focus/blur animations
// ===============================================================

cadolines.Hexagon = function(data, group) {
  var instance = this;
  this.group = group; // parent group
  this.style = {
    decorationScale: 1
  };

  var hexagonSize = cadolines.current.groups.hexagonSize;

  // create the hexagon shape
  this.hexagonShape = new paper.Path.RegularPolygon(new paper.Point(0, 0), 6, hexagonSize);
  this.hexagonShape.style = {
    fillColor: "#4855a4",
    shadowColor: cadolines.current.groups.shadowColor
  };

  // create a title text
  var titleFontSize = cadolines.current.groups.hexagonTextSize;
  this.hexagonTitle = new paper.PointText(new paper.Point(-0.62 * hexagonSize, titleFontSize));
  this.hexagonTitle.content = data.title;
  this.hexagonTitle.style = {
    fontFamily: cadolines.current.groups.hexagonFont,
    fontSize: titleFontSize
  }
  // center vertically
  this.hexagonTitle.translate(0, -this.hexagonTitle.bounds.height / 2 + titleFontSize / 6);

  // create see more text
  var seeMoreFontSize = 28/46 * cadolines.current.groups.hexagonTextSize / cadolines.current.groups.focusScale;
  this.hexagonMore = new paper.PointText(new paper.Point(-0.62 * hexagonSize, this.hexagonTitle.bounds.bottom + seeMoreFontSize));
  // this.hexagonMore.content = "CLICK TO SEE MORE";
  this.hexagonMore.style = {
    fontFamily: cadolines.current.groups.hexagonFont,
    fontSize: seeMoreFontSize
  }

  // create hexagon decoration
  var firstPoint = new paper.Point(0, hexagonSize * Math.sqrt(3)/2).rotate(-30);
  var secondPoint = new paper.Point(0, hexagonSize);
  this.hexagonDecoration = new paper.Path();
  this.hexagonDecoration.strokeColor = '#4855a4';
  this.hexagonDecoration.add(firstPoint);
  this.hexagonDecoration.add(secondPoint);
  this.hexagonDecoration.add(secondPoint.rotate(60));
  this.hexagonDecoration.add(secondPoint.rotate(120));
  this.hexagonDecoration.add(secondPoint.rotate(180));
  this.hexagonDecoration.add(firstPoint.rotate(-120));
  this.hexagonDecoration.pivot = new paper.Point(0, 0);
  this.hexagonDecoration.scale(0.98);

  // create a paper group for shape & texts
  this.paperGroup = new paper.Group([
    this.hexagonDecoration,
    this.hexagonShape,
    this.hexagonTitle,
    this.hexagonMore
  ]);
}

// set focus animation progress
cadolines.Hexagon.prototype.setFocusAt = function(progress) {
  // change opacity
  var currentOpacity = cadolines.interpolateValues(cadolines.current.groups.idleOpacity, 1, progress);
  this.hexagonShape.opacity = currentOpacity;
  this.hexagonMore.opacity = progress;

  // change text color
  var idleColor = cadolines.current.groups.idleTextColor;
  var focusColor = cadolines.current.groups.focusTextColor;
  this.hexagonTitle.fillColor = cadolines.interpolateColors(idleColor, focusColor, progress);
  this.hexagonMore.fillColor = cadolines.interpolateColors(idleColor, focusColor, progress);

  // update decoration
  if (cadolines.isMobile()) {
    this.hexagonDecoration.visible = false;
  } else {
    this.hexagonDecoration.visible = true;

    var groupScale = this.group.animation.getRelativeScale(this.group.style.scale);
    var decorationScale = cadolines.interpolateValues(1, 1.175, progress);
    this.hexagonDecoration.strokeWidth = groupScale * 4;
    cadolines.rescale(this.style, this.hexagonDecoration, decorationScale, "decorationScale");
  };
}

// set blur animation progress
cadolines.Hexagon.prototype.setBlurAt = function(progress) {
  // change opacity
  var hexagonOpacity = cadolines.interpolateValues(cadolines.current.groups.idleOpacity, cadolines.current.groups.blurOpacity, progress);
  this.hexagonShape.opacity = hexagonOpacity;
  this.hexagonTitle.opacity = 1 - progress;

  // hide decoration
  this.hexagonDecoration.visible = false;
}

// update shadow
cadolines.Hexagon.prototype.setShadow = function(shadowOffset, shadowBlur) {
  this.hexagonShape.style.shadowOffset = shadowOffset;
  this.hexagonShape.style.shadowBlur = shadowBlur;
}


// ===============================================================
// TRIANGLE CLASS
// - loads the triangle image, applies the blue filters, adds text
// - mouse hover animation, open a webpage on click
// ===============================================================

cadolines.Triangle = function(data, group, index) {
  var self = this;
  this.group = group; // parent group
  this.index = index; // which groups is this?

  var trianglesConfig = cadolines.current.triangles;

  // create a mask
  this.maskTriangle = new paper.Path.RegularPolygon(new paper.Point(0, 0), 3, trianglesConfig.size);
  this.maskTriangle.pivot = new paper.Point(0, 0);
  this.maskTriangle.rotate(trianglesConfig.rotations[this.index]);

  // those will be populated with image states
  this.normalImage = new paper.Group();
  this.blueImage = new paper.Group();

  // create an image mask, make it larger
  self.imageMask = self.maskTriangle.clone().scale(cadolines.current.groups.focusScale);
  self.imageMask.fillColor = "white";
  self.rasterGroup = new paper.Group([self.imageMask]);
  self.rasterGroup.visible = false;

  // load raster image
  this.imageRaster = new paper.Raster(data.image);
  self.imageRaster.visible = false;
  self.imageRaster.blendMode = "source-in";
  this.imageRaster.onLoad = function() {

    // resize image, make it visible
    self.imageRaster.fitBounds(self.imageMask.bounds, true);
    self.imageRaster.visible = true;
    self.rasterGroup.visible = true;

    // create normal state raster
    self.rasterGroup.addChild(self.imageRaster);
    self.normalImageRaster = self.rasterGroup.rasterize();
    self.normalImageRaster.scale(1/cadolines.current.groups.focusScale);
    self.normalImage.addChild(self.normalImageRaster);

    // add filters
    self.saturationFilter = self.imageMask.clone();
    self.saturationFilter.fillColor = "rgba(20,0,0)";
    self.saturationFilter.blendMode = "saturation";
    self.rasterGroup.addChild(self.saturationFilter);

    self.lightnessFilter = self.imageMask.clone();
    self.lightnessFilter.fillColor = "rgba(220, 220, 220)";
    self.lightnessFilter.blendMode = "soft-light";
    self.rasterGroup.addChild(self.lightnessFilter);

    self.colorFilter = self.imageMask.clone();
    self.colorFilter.fillColor = "#2f4eec";
    self.colorFilter.blendMode = "color";
    self.colorFilter.opacity = 0.3;
    self.rasterGroup.addChild(self.colorFilter);

    // safari has troubles with blend modes
    if (cadolines.isSafari || cadolines.isIE) {
      self.lightnessFilter.visible = false;
      self.saturationFilter.visible = false;
      self.colorFilter.visible = false;
    };

    // create blue state raster
    self.blueImageRaster = self.rasterGroup.rasterize();
    self.blueImageRaster.scale(1/cadolines.current.groups.focusScale);
    self.blueImage.addChild(self.blueImageRaster);

    self.rasterGroup.remove();
  }

  // create a text
  var titleJustification, titlePosition;
  var titleFontSize = 14;
  var titleDistanceHorizontal = 1/2 * trianglesConfig.size - titleFontSize;
  var titleDistanceVertical = 3/2 * titleFontSize;
  if(trianglesConfig.rotations[this.index] < 0) {
    titleJustification = "left";
    titlePosition = new paper.Point(-titleDistanceHorizontal, titleDistanceVertical);
  } else {
    titleJustification = "right";
    titlePosition = new paper.Point(titleDistanceHorizontal, titleDistanceVertical);
  }

  this.triangleTitle = new paper.PointText(titlePosition);
  this.triangleTitle.content = data.title;
  this.triangleTitle.style = {
    fillColor: "white",
    fontFamily: cadolines.current.groups.triangleFont,
    fontSize: titleFontSize,
    fontWeight: 700,
    justification: titleJustification
  }
  this.triangleTitle.translate(0, -this.triangleTitle.bounds.height / 2);

  // create a white border
  this.borderTriangle = this.maskTriangle.clone();
  this.borderTriangle.strokeColor = "white";
  this.borderTriangle.strokeWidth = 5 * cadolines.current.groups.focusScale * this.group.animation.style.scale;
  this.borderTriangle.visible = false;

  // create a hover trigger
  this.hoverTrigger = this.maskTriangle.clone();
  this.hoverTrigger.fillColor = "rgba(0,0,0,0)";

  // create a shadow triangle
  this.triangleShadow = this.hoverTrigger.clone();
  this.triangleShadow.style = {
    fillColor: "white",
    shadowColor: cadolines.current.groups.shadowColor
  };
  this.triangleShadow.scale(0.98, new paper.Point(0, 0));

  // create a paper group for triangle elements
  this.paperGroup = new paper.Group([
    // this.maskTriangle,
    this.rasterGroup,
    this.normalImage,
    this.blueImage,
    this.triangleTitle,
    this.borderTriangle,
  ]);
  // this.paperGroup.clipped = true;


  // set position
  this.paperGroup.pivot = new paper.Point(0, 0);
  this.hoverTrigger.pivot = new paper.Point(0, 0);
  this.triangleShadow.pivot = new paper.Point(0, 0);

  this.resetPosition();


  // hover animation
  this.hoverTrigger.on("mouseenter", function(){
    self.animateMouseEnter();
  });
  this.hoverTrigger.on("mouseleave", function(){
    self.animateMouseLeave();
  });

  // click to navigate somewhere
  function triangleClick() {
    window.open(data.link, "_blank");
  }

  this.hoverTrigger.on("mousedown", cadolines.mouseDown);

  this.hoverTrigger.on("mouseup", function(ev){
    cadolines.mouseUp.call(this, ev, triangleClick);
  });
}

// send shadow to back, bring trigger to front, set initial state
cadolines.Triangle.prototype.init = function() {
  // first bring the trigger to front scene,
  // so it won't be obstructed by other triangle elements
  this.hoverTrigger.bringToFront();
  this.triangleShadow.sendToBack();

  // set initial state
  this.setOpenAt(0);
  this.setHoverAt(0);
}

// reset position when going from mobile to desktop or vice-versa
cadolines.Triangle.prototype.resetPosition = function() {
  var trianglesConfig = cadolines.current.triangles;
  var groupScale = this.group.style.scale;
  var newPosition = this.group.paperGroup.position.add(trianglesConfig.positions[this.index].multiply(groupScale));

  // translate the group, the trigger and the shadow
  this.paperGroup.position = newPosition;
  this.hoverTrigger.position = newPosition;
  this.triangleShadow.position = newPosition;
}

// enter animation
cadolines.Triangle.prototype.animateMouseEnter = function() {
  cadolines.animate(this, "hover", {
    from: 0,
    to: 1,
    duration: 200,
    onUpdate: this.setHoverAt,
    resume: true
  });
}

// exit animation
cadolines.Triangle.prototype.animateMouseLeave = function() {
  cadolines.animate(this, "hover", {
    from: 1,
    to: 0,
    duration: 200,
    onUpdate: this.setHoverAt,
    resume: true
  });
}

// set fade animation progress
cadolines.Triangle.prototype.setOpenAt = function(progress) {
  this.paperGroup.opacity = progress;
  this.paperGroup.visible = progress > 0;

  this.hoverTrigger.visible = progress > 0;

  this.triangleShadow.opacity = progress;
  this.triangleShadow.visible = progress > 0;
}

// set hover animation progress
cadolines.Triangle.prototype.setHoverAt = function(t) {
  if (cadolines.isMobile()) {
    this.blueImage.opacity = 0;
    this.borderTriangle.opacity = 0;
    this.triangleTitle.fillColor.alpha = 1;
  } else {
    this.blueImage.opacity = 1 - t;
    this.borderTriangle.opacity = t;
    this.triangleTitle.fillColor.alpha = t;
  }
}

// update shadow
cadolines.Triangle.prototype.setShadow = function(shadowOffset, shadowBlur) {
  this.triangleShadow.style.shadowOffset = shadowOffset;
  this.triangleShadow.style.shadowBlur = shadowBlur;
}



// ===============================================================
// LOGO CLASS
// - creates a logo, applies proper scale and position
// ===============================================================

cadolines.Logo = function(animation) {
  var self = this;
  this.animation = animation; // parent animation
  this.style = {
    scale: 1
  };

  // create the paper raster object
  this.paperItem = new paper.Raster("img/main_logo.png");
  this.paperItem.visible = false;
  this.paperItem.onLoad = function() {
    self.paperItem.visible = true;

    // initial autoscale
    self.autoscale();

    // update on window resize
    paper.view.on("resize", function() {
      self.autoscale();
    }, true);
  }

}

// apply new scale
cadolines.Logo.prototype.rescale = function(newScale) {
  cadolines.rescale(this.style, this.paperItem, newScale);
}

// update scale/position on resize
cadolines.Logo.prototype.autoscale = function() {
  if(cadolines.isMobile()) {
    this.paperItem.visible = false;
  } else {
    this.paperItem.visible = true;

    // apply animation scale
    var newScale = this.animation.getRelativeScale(1);
    this.rescale(newScale);

    // apply scaled position
    this.paperItem.position.x = paper.view.bounds.center.x;
    this.paperItem.position.y = this.paperItem.bounds.height / 2;
  }

}


// ===============================================================
// CLOSE BUTTON CLASS
// - creates a close button, adds animations and autoscale
// ===============================================================

cadolines.CloseButton = function(animation) {
  var self = this;
  this.animation = animation; // parent animation
  this.style = {
    scale: 1
  }

  var xBarLength = 54;

  // create the x icon
  this.xBar1 = new paper.Shape.Rectangle(new paper.Point(0, -xBarLength/2), new paper.Size(6, xBarLength));
  this.xBar1.fillColor = "black"
  this.xBar1.rotate(45);

  this.xBar2 = this.xBar1.clone().rotate(-90);

  // create the close text
  this.closeText = new paper.PointText(2/3 * xBarLength, 10);
  this.closeText.content = "close";
  this.closeText.style = {
    fillColor: "black",
    fontFamily: cadolines.current.groups.hexagonFont,
    fontSize: 28
  };

  // create a paper group
  this.paperGroup = new paper.Group([
    this.xBar1,
    this.xBar2,
    this.closeText
  ]);

  // create a trigger
  this.clickTrigger = new paper.Shape.Rectangle(this.paperGroup.bounds);
  this.clickTrigger.fillColor = "rgba(0,0,0,0)";
  this.clickTrigger.scale(1.2);
  this.paperGroup.addChild(this.clickTrigger);

  // initial autoscale
  this.autoscale();

  // update on window resize
  paper.view.on("resize", function() {
    self.autoscale();
  }, true);

  // click -> close focused group
  this.paperGroup.on("click", function(){
    if(self.animation.focusedGroup) {
      self.animation.closeFocusedGroup();
    }
  });

  // parallax
  this.animation.onParallax(function(){
    // do this only if animation is not in progress
    if (!self.tweens || !self.tweens.enter) {
      self.autoscale();
    };
  });

}

// applies a new scale
cadolines.CloseButton.prototype.rescale = function(newScale) {
  cadolines.rescale(this.style, this.paperGroup, newScale);
}

// update scale/position on resize
cadolines.CloseButton.prototype.autoscale = function() {
  // apply animation scale
  var newScale = this.animation.getRelativeScale(1);
  this.rescale(newScale);

  // apply scaled position
  var newPosition = this.animation.getRelativePoint(cadolines.current.close.position);
  var parallaxMove = this.animation.parallaxMove.multiply(Math.pow(cadolines.current.groups.focusScale, 3));
  this.paperGroup.position = newPosition.add(parallaxMove);
}

// set enter animation progress
cadolines.CloseButton.prototype.setOpenAt = function(progress) {
  this.paperGroup.opacity = progress;
  this.paperGroup.visible = progress > 0;
}

// enter animation
cadolines.CloseButton.prototype.animateEnter = function() {
  cadolines.animate(this, "enter", {
    from: 0,
    to: 1,
    duration: 400,
    onUpdate: this.setOpenAt,
    resume: true
  });
}

// leave animation
cadolines.CloseButton.prototype.animateLeave = function() {
  cadolines.animate(this, "enter", {
    from: 1,
    to: 0,
    duration: 400,
    onUpdate: this.setOpenAt,
    resume: true
  });
}


// ===============================================================
// BACKGROUND CLASS
// - creates the pattern and light circle
// ===============================================================

cadolines.Background = function(animation) {
  var self = this;
  this.animation = animation; // parent animation
  this.groupStyle = {
    scale: 1
  }
  this.circleStyle = {
    scale: 1
  }

  // create a group to contain all symbols
  this.paperGroup = new paper.Group();

  // create the white overlapping circle
  this.lightCircle = new paper.Path.Circle({
    center: paper.view.center,
    radius: 1920
  });

  this.lightCircle.fillColor = {
    gradient: {
        stops: [["rgba(255, 255, 255, 1)", 0.05], ["rgba(255, 255, 255, 0)", 1]],
        radial: true
    },
    origin: this.lightCircle.position,
    destination: this.lightCircle.bounds.rightCenter
  };

  // load bg image, create a symbol and initialize
  this.imageRaster = new paper.Raster("img/bg_pattern.png");
  this.imageRaster.visible = false;
  this.imageRaster.onLoad = function() {
    // create symbol, make visible
    self.symbol = new paper.Symbol(self.imageRaster);
    self.symbol.definition.visible = true;
    self.symbol.definition.opacity = 0.4;

    // initial autoscale
    self.autoscale();

    // update on window resize
    paper.view.on("resize", function() {
      self.autoscale();
    }, true);
  }
}

// apply a new scale
cadolines.Background.prototype.rescale = function(newScale) {
  cadolines.rescale(this.groupStyle, this.symbol.definition, newScale);
  cadolines.rescale(this.circleStyle, this.lightCircle, newScale);
}

// generate the pattern again on resize, update light circle position
cadolines.Background.prototype.autoscale = function() {
  var bgWidth = this.animation.getRelativeScale(35);
  var bgHeight = this.animation.getRelativeScale(68);

  // apply animation scale
  var newScale = this.animation.getRelativeScale(1);
  this.rescale(newScale);

  // empty group
  this.paperGroup.removeChildren();

  // create new symbol copies
  var symbolsHorizontal = Math.floor(paper.view.bounds.width / bgWidth) + 2;
  var symbolsVertical = Math.floor(paper.view.bounds.height / bgHeight) + 2;

  for (var i = 0; i < symbolsHorizontal; i++) {
    for (var j = 0; j < symbolsVertical; j++) {
      var placed = this.symbol.place(new paper.Point(i * bgWidth, j * bgHeight + Math.pow(-1, i) * bgHeight / 4));
      this.paperGroup.addChild(placed);
    };
  };

  // reposition light circle
  this.lightCircle.position.x = paper.view.bounds.center.x;
  this.lightCircle.position.y = paper.view.bounds.top + this.animation.getRelativeScale(100);
}

