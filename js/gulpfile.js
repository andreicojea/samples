var gulp = require('gulp');
var usemin = require('gulp-usemin');
var clean = require('gulp-clean');
var uglify = require('gulp-uglify');
var minifyCss = require('gulp-minify-css');
var rev = require('gulp-rev');
var print = require('gulp-print');
var replace = require('gulp-replace');
var rename = require('gulp-rename');
var runSequence = require('run-sequence');

gulp.task('clean', function () {
  return gulp.src('./build', {read: false})
    .pipe(clean());
});

gulp.task('usemin', function () {
  return gulp.src('./src/index.html')
    .pipe(usemin({
      css: [ minifyCss(), rev() ],
      js: [ uglify(), rev() ],
      html: [rename({
        basename: "front_page_layout",
        extname: ".php"
      })]
    }))
    .pipe(replace('src="front_page', 'src="<?php echo sfConfig::get(\'app_static_route\') ?>/front_page'))
    .pipe(replace('href="front_page', 'href="<?php echo sfConfig::get(\'app_static_route\') ?>/front_page'))
    .pipe(replace('"cadolines.json"', '"<?php echo url_for(\'front_page_json\') ?>"'))
    .pipe(replace('url("img/', 'url("../../img/'))
    .pipe(replace('url(img/', 'url(../../img/'))
    .pipe(replace('url(\'fonts/', 'url(\'../fonts/'))
    .pipe(replace('url(fonts/', 'url(../fonts/'))
    .pipe(replace('"bower_components/TurnWheel/jReject/images/"', '"../../img/jReject/images/"'))
    .pipe(gulp.dest('build/'));
});

gulp.task('fonts', function () {
  return gulp.src('./src/fonts/**/*')
    .pipe(gulp.dest('build/front_page/fonts/'));
});

gulp.task('images', function () {
  return gulp.src([
      './src/img/**/*',
      '!./src/img/triangles/**/*'
    ])
    .pipe(gulp.dest('build/img'));
});

gulp.task('default', function(done){
  runSequence('clean', 'usemin', 'fonts', 'images', done);
});